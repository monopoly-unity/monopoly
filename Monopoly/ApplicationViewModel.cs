﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Monopoly
{
    public class ApplicationViewModel : ObservableObject
    {
        StartViewModel startViewModel = new StartViewModel();
        SettingsViewModel settingsViewModel = new SettingsViewModel();
        NewGameViewModel newGameViewModel = new NewGameViewModel();
        GameViewModel gameViewModel = new GameViewModel();

        public ApplicationViewModel()
        {
            CurrentWorkspace = startViewModel;

            GoToSettingsCommand = new RelayCommand((p) => GoToSettings());
            GoToStartCommand = new RelayCommand((p) => GoToStart());
            GoToNewGameCommand = new RelayCommand((p) => GoToNewGame());
            GoToGameCommand = new RelayCommand((p) => GoToGame());
        }

        private WorkspaceViewModel currentWorkspace;
        public WorkspaceViewModel CurrentWorkspace
        {
            get { return currentWorkspace; }
            set
            {
                if (currentWorkspace != value)
                {
                    currentWorkspace = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand GoToSettingsCommand { get; set; }
        public ICommand GoToStartCommand { get; set; }
        public ICommand GoToNewGameCommand { get; set; }
        public ICommand GoToGameCommand { get; set; }

        public void GoToSettings()
        {
            CurrentWorkspace = settingsViewModel;
        }

        public void GoToStart()
        {
            CurrentWorkspace = startViewModel;
        }

        public void GoToNewGame()
        {
            CurrentWorkspace = newGameViewModel;
        }

        public void GoToGame()
        {
            CurrentWorkspace = gameViewModel;
        }
    }
}
