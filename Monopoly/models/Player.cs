﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Monopoly.models
{
    class Player
    {
        public string name;
        public Image figure;

        public Player(string name, String imagePath, double x = 700d, double y = 700d)
        {
            this.name = name;
            createFigure(imagePath, x, y);
        }

        private void createFigure(String imagePath, double x, double y)
        {
            figure = new Image();
            figure.Source = new BitmapImage(new Uri(imagePath, UriKind.Relative));
            figure.Width = 30;
            figure.Height = 30;
            Canvas.SetLeft(figure, x);
            Canvas.SetTop(figure, y);
            Canvas.SetZIndex(figure, 10);
        }

        public void printPlayer(Canvas board)
        {
            board.Children.Add(figure);
        }
    }
}
