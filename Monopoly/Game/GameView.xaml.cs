﻿using Monopoly.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Monopoly
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class GameView : UserControl
    {
        Player p1 = new Player("Felix", "/images/Hut_top.png", 700, 700);

        public GameView()
        {
            InitializeComponent();
            p1.printPlayer(boardCanvas);

        }

        private void Reset(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(p1.figure, 700);
            Canvas.SetTop(p1.figure, 700);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double boardWidth = Board.ActualWidth;
            double boardHeight = Board.ActualHeight;

            double x = Canvas.GetLeft(p1.figure);
            double y = Canvas.GetTop(p1.figure);

            TranslateTransform trans = new TranslateTransform();

            p1.figure.RenderTransform = trans;

            DoubleAnimation anim = new DoubleAnimation();
            anim.FillBehavior = FillBehavior.Stop;
            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseInOut;
            anim.EasingFunction = ease;
            anim.Duration = TimeSpan.FromSeconds(0.3);
            
            Debug.WriteLine("board: " + boardWidth + "," + boardHeight + " x: " + x + " y:" + x);

            //Bottom to the Left
            if (x - 100 >= 0 && y == 700 && x != 0)
            {
                anim.To = -100;
                anim.Completed += (s, eventArgs) =>
                {
                    anim.BeginAnimation(TranslateTransform.XProperty, null);
                    Canvas.SetLeft(p1.figure, x - 100);
                    Debug.WriteLine(Canvas.GetLeft(p1.figure));
                };
                trans.BeginAnimation(TranslateTransform.XProperty, anim);
                Debug.WriteLine("Bottom to the Left");
            }
            
            //Bottom Left to Top
            if (y - 100 >= 0 && x == 0 && y != 0)
            {
                anim.To = -100;
                anim.Completed += (s, eventArgs) =>
                {
                    anim.BeginAnimation(TranslateTransform.YProperty, null);
                    Canvas.SetTop(p1.figure, y - 100);
                    Debug.WriteLine(Canvas.GetTop(p1.figure));
                };
                trans.BeginAnimation(TranslateTransform.YProperty, anim);
                Debug.WriteLine("Bottom Left to Top");
            }

            //Top Left To Right
            if (x + 100 <= boardWidth && y == 0)
            {
                anim.To = 100;
                anim.Completed += (s, eventArgs) =>
                {
                    anim.BeginAnimation(TranslateTransform.XProperty, null);
                    Canvas.SetLeft(p1.figure, x + 100);
                    Debug.WriteLine(Canvas.GetLeft(p1.figure));
                };
                trans.BeginAnimation(TranslateTransform.XProperty, anim);
                Debug.WriteLine("Bottom to the Left");
            }

            //Bottom Left to Top
            if (y + 100 <= boardHeight && x == 700)
            {
                anim.To = 100;
                anim.Completed += (s, eventArgs) =>
                {
                    anim.BeginAnimation(TranslateTransform.YProperty, null);
                    Canvas.SetTop(p1.figure, y + 100);
                    Debug.WriteLine(Canvas.GetTop(p1.figure));
                };
                trans.BeginAnimation(TranslateTransform.YProperty, anim);
                Debug.WriteLine("Bottom Left to Top");
            }



            //DoubleAnimation anim2 = new DoubleAnimation(left, left + 50, TimeSpan.FromSeconds(1));

            //trans.BeginAnimation(TranslateTransform.YProperty, anim2);
        }

        void FinishedAnimationY(object sender, EventArgs e)
        {

            double y = Canvas.GetTop(p1.figure);
            p1.figure.BeginAnimation(TranslateTransform.YProperty, null);
            Canvas.SetTop(p1.figure, y + 100);
            Debug.WriteLine(Canvas.GetTop(p1.figure));
        }
    }
}
